package tdlm.controller;

import saf.AddingNewItem;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javax.imageio.ImageIO;
import properties_manager.PropertiesManager;
import tdlm.data.DataManager;
import tdlm.gui.Workspace;
import saf.AppTemplate;


import static saf.settings.AppPropertyType.SAVE_UNSAVED_WORK_MESSAGE;
import static saf.settings.AppPropertyType.SAVE_UNSAVED_WORK_TITLE;
import saf.ui.AppMessageDialogSingleton;
import saf.ui.AppYesNoCancelDialogSingleton;
import tdlm.PropertyType;
import tdlm.data.ToDoItem;

/**
 * This class responds to interactions with todo list editing controls.
 * 
 * @author McKillaGorilla
 * @version 1.0
 */
public class ToDoListController {
    AppTemplate app;
    
    public ToDoListController(AppTemplate initApp) {
	app = initApp;
    }
    
    public void processAddName(String s)
    {
        //Workspace workspace = (Workspace)app.getWorkspaceComponent();
        DataManager dataManager = (DataManager)app.getDataComponent();
        
        dataManager.setName(s);
    }
    
    public void processAddOwner(String s)
    {
        //Workspace workspace = (Workspace)app.getWorkspaceComponent();
        DataManager dataManager = (DataManager)app.getDataComponent();
        
        dataManager.setOwner(s);
    }
    
    public void processAddItem() 
    {	
	// ENABLE/DISABLE THE PROPER BUTTONS
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
	
        
        DataManager dataManager = (DataManager)app.getDataComponent();
        
        //PropertiesManager props = PropertiesManager.getPropertiesManager();
        AddingNewItem NewItemDialog = AddingNewItem.getSingleton();
        NewItemDialog.show("Adding New Item");
        
        //CREATE TO DO ITEMS WITH USER INPUTS
        ToDoItem item = new ToDoItem();
            item.setCategory(NewItemDialog.category.getText());
            item.setDescription(NewItemDialog.description.getText());
            item.setStartDate(NewItemDialog.startdate.getValue());
            item.setEndDate(NewItemDialog.enddate.getValue());
            item.setCompleted(NewItemDialog.complete.isSelected());
            
        
        //CHECK WHAT THE USER SELECT
        String selection = NewItemDialog.getSelection();
        if (selection.equals(AddingNewItem.OK))
        {
            dataManager.addItem(item);
            NewItemDialog.reset();
            selection = "";
        }


        
        if(dataManager.getList().isEmpty())
            workspace.setRemoveButton(true);
        else
            workspace.setRemoveButton(false);
        

    }
    
    public void processRemoveItem() 
    {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        DataManager dataManager = (DataManager)app.getDataComponent();
        dataManager.getList().remove(workspace.getTable().getSelectionModel().getSelectedItem());
        if(dataManager.getList().isEmpty())
            workspace.setRemoveButton(true);
        else
            workspace.setRemoveButton(false);
        

        workspace.getTable().getSelectionModel().clearSelection();
        

    }
    
    public void processMoveUpItem() 
    {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        DataManager dataManager = (DataManager)app.getDataComponent();

        String tempCat = "";
        String tempDes = "";
        LocalDate tempStart = null;
        LocalDate tempEnd = null;
        boolean tempCom = true;
        
        ToDoItem currentItem = workspace.getTable().getSelectionModel().getSelectedItem();
        int index = dataManager.getList().indexOf(workspace.getTable().getSelectionModel().getSelectedItem());
        
        tempCat = currentItem.getCategory();
        tempDes = currentItem.getDescription();
        tempStart = currentItem.getStartDate();
        tempEnd = currentItem.getEndDate();
        tempCom = currentItem.getCompleted();
        
 
        ToDoItem swapItem = dataManager.getList().get(index-1);
        
        currentItem.setCategory(swapItem.getCategory());
        currentItem.setDescription(swapItem.getDescription());
        currentItem.setStartDate(swapItem.getStartDate());
        currentItem.setEndDate(swapItem.getEndDate());
        currentItem.setCompleted(swapItem.getCompleted());
        
        swapItem.setCategory(tempCat);
        swapItem.setDescription(tempDes);
        swapItem.setStartDate(tempStart);
        swapItem.setEndDate(tempEnd);
        swapItem.setCompleted(tempCom);
 
        workspace.getTable().getSelectionModel().clearSelection();  
    }
    
    public void processMoveDownItem() 
    {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        DataManager dataManager = (DataManager)app.getDataComponent();

        String tempCat = "";
        String tempDes = "";
        LocalDate tempStart = null;
        LocalDate tempEnd = null;
        boolean tempCom = true;
        
        ToDoItem currentItem = workspace.getTable().getSelectionModel().getSelectedItem();
        int index = dataManager.getList().indexOf(workspace.getTable().getSelectionModel().getSelectedItem());
        
        tempCat = currentItem.getCategory();
        tempDes = currentItem.getDescription();
        tempStart = currentItem.getStartDate();
        tempEnd = currentItem.getEndDate();
        tempCom = currentItem.getCompleted();
        
 
        ToDoItem swapItem = dataManager.getList().get(index+1);
        
        currentItem.setCategory(swapItem.getCategory());
        currentItem.setDescription(swapItem.getDescription());
        currentItem.setStartDate(swapItem.getStartDate());
        currentItem.setEndDate(swapItem.getEndDate());
        currentItem.setCompleted(swapItem.getCompleted());
        
        swapItem.setCategory(tempCat);
        swapItem.setDescription(tempDes);
        swapItem.setStartDate(tempStart);
        swapItem.setEndDate(tempEnd);
        swapItem.setCompleted(tempCom);
 
        workspace.getTable().getSelectionModel().clearSelection();
        
    }
    
    public void processEditItem() 
    {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        DataManager dataManager = (DataManager)app.getDataComponent();
        
        AddingNewItem NewItemDialog = AddingNewItem.getSingleton();
        
        ToDoItem currentItem = workspace.getTable().getSelectionModel().getSelectedItem();
        
        NewItemDialog.category.setText(workspace.getTable().getSelectionModel().getSelectedItem().getCategory());
        NewItemDialog.description.setText(workspace.getTable().getSelectionModel().getSelectedItem().getDescription());
        NewItemDialog.startdate.setValue(workspace.getTable().getSelectionModel().getSelectedItem().getStartDate());
        NewItemDialog.enddate.setValue(workspace.getTable().getSelectionModel().getSelectedItem().getEndDate());
        NewItemDialog.complete.setSelected(workspace.getTable().getSelectionModel().getSelectedItem().getCompleted());

        


        NewItemDialog.show("Editing");
        
        
        String selection = NewItemDialog.getSelection();
        if (selection.equals(AddingNewItem.OK))
        {
            currentItem.setCategory(NewItemDialog.category.getText());
            currentItem.setDescription(NewItemDialog.description.getText());
            currentItem.setStartDate(NewItemDialog.startdate.getValue());
            currentItem.setEndDate(NewItemDialog.enddate.getValue());
            currentItem.setCompleted(NewItemDialog.complete.isSelected());
            //dataManager.addItem(currentItem);
            NewItemDialog.reset();
        }
        
        if(dataManager.getList().isEmpty())
            workspace.setRemoveButton(true);
        else
            workspace.setRemoveButton(false);
        
        NewItemDialog.reset();
        workspace.getTable().getSelectionModel().clearSelection();
    }
}
