package saf;

import java.time.LocalDate;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.scene.control.DatePicker;
import properties_manager.PropertiesManager;
import saf.components.AppStyleArbiter;
import static saf.components.AppStyleArbiter.BUTTONS;
import static saf.components.AppStyleArbiter.CLASS_BORDERED_PANE;
import static saf.components.AppStyleArbiter.CLASS_FILE_BUTTON;
import static saf.settings.AppPropertyType.CATEGORY_COLUMN_HEADING;
import static saf.settings.AppPropertyType.COMPLETED_COLUMN_HEADING;
import static saf.settings.AppPropertyType.DESCRIPTION_COLUMN_HEADING;
import static saf.settings.AppPropertyType.END_DATE_COLUMN_HEADING;
import static saf.settings.AppPropertyType.PROPERTIES_LOAD_ERROR_MESSAGE;
import static saf.settings.AppPropertyType.PROPERTIES_LOAD_ERROR_TITLE;
import static saf.settings.AppPropertyType.START_DATE_COLUMN_HEADING;
import static saf.settings.AppStartupConstants.CLOSE_BUTTON_LABEL;
import static saf.settings.AppStartupConstants.OK_BUTTON_LABEL;
import static saf.settings.AppStartupConstants.PATH_DATA;
import static saf.settings.AppStartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import static saf.settings.AppStartupConstants.SIMPLE_APP_PROPERTIES_FILE_NAME;
import static saf.settings.AppStartupConstants.WORKSPACE_PROPERTIES_FILE_NAME;
import saf.ui.AppMessageDialogSingleton;
import xml_utilities.InvalidXMLFileFormatException;



public class AddingNewItem extends Stage implements AppStyleArbiter{
    // HERE'S THE SINGLETON
    static AddingNewItem singleton;
    
    // GUI CONTROLS FOR OUR DIALOG
    
    GridPane gridpane = new GridPane();
    Scene messageScene;
    protected Button okButton;
    Button cancelButton;
    String selection;
    public TextField category = new TextField();
    public TextField description = new TextField();
    public DatePicker startdate = new DatePicker();
    public DatePicker enddate = new DatePicker();
    public CheckBox complete = new CheckBox();
    static final String BUTTON = "button";
    public static final String OK = "Ok";
    public static final String CANCEL = "Cancel";
    

    private AddingNewItem() {}
    

    public static AddingNewItem getSingleton() {
	if (singleton == null)
	    singleton = new AddingNewItem();
	return singleton;
    }

    public void init(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        okButton = new Button(OK_BUTTON_LABEL);
        cancelButton = new Button(CLOSE_BUTTON_LABEL);
        cancelButton.setOnAction(e->{ AddingNewItem.this.close(); });

        EventHandler yesNoCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            AddingNewItem.this.selection = sourceButton.getText();
            AddingNewItem.this.hide();
        };
        
        
        okButton.setOnAction(yesNoCancelHandler);
        cancelButton.setOnAction(yesNoCancelHandler);
        
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();

         boolean success = loadProperties(SIMPLE_APP_PROPERTIES_FILE_NAME)
		    && loadProperties(WORKSPACE_PROPERTIES_FILE_NAME);  
	if (success) {
        
        startdate.setValue(LocalDate.now());
        enddate.setValue(LocalDate.now());
        gridpane.setHgap(10);
        gridpane.setVgap(10);
        String CATEGORY = props.getProperty(CATEGORY_COLUMN_HEADING);
        String DESCRIPTION = props.getProperty(DESCRIPTION_COLUMN_HEADING);
        String STARTDATE = props.getProperty(START_DATE_COLUMN_HEADING);
        String ENDDATE = props.getProperty(END_DATE_COLUMN_HEADING);
        String COMPLETED = props.getProperty(COMPLETED_COLUMN_HEADING);

        gridpane.add(new Label(CATEGORY),0,0);
        gridpane.add(category,1,0);
        gridpane.add(new Label(DESCRIPTION),0,1);
        gridpane.add(description,1,1);
        gridpane.add(new Label(STARTDATE),0,2);
        gridpane.add(startdate,1,2);
        gridpane.add(new Label(ENDDATE),0,3);
        gridpane.add(enddate,1,3);
        gridpane.add(new Label(COMPLETED),0,4);
        gridpane.add(complete,1,4);
        
        gridpane.add(okButton,1,5);
        gridpane.add(cancelButton,1,5);
        gridpane.setHalignment(okButton, HPos.CENTER);
        gridpane.setHalignment(cancelButton, HPos.RIGHT);
        gridpane.setPadding(new Insets(40, 60, 40, 60));
        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(gridpane);
        this.setScene(messageScene);
        

        }
    }


    public String getSelection() {
        return selection;
    }

    
    public void reset()
    {
        category.setText("");
        description.setText("");
        startdate.setValue(LocalDate.now());
        enddate.setValue(LocalDate.now());
        complete.setSelected(false);
    }
    public void show(String title) {
	// SET THE DIALOG TITLE BAR TITLE
	setTitle(title);
        showAndWait();
    }
    public boolean loadProperties(String propertiesFileName) {
	    PropertiesManager props = PropertiesManager.getPropertiesManager();
	try {
	    // LOAD THE SETTINGS FOR STARTING THE APP
	    props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
	    props.loadProperties(propertiesFileName, PROPERTIES_SCHEMA_FILE_NAME);
	    return true;
	} catch (InvalidXMLFileFormatException ixmlffe) {
	    // SOMETHING WENT WRONG INITIALIZING THE XML FILE
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(PROPERTIES_LOAD_ERROR_TITLE), props.getProperty(PROPERTIES_LOAD_ERROR_MESSAGE));
	    return false;
	}
    }
    
    public void initStyle() {
	
	okButton.getStyleClass().add(BUTTONS);

    }

}