/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;

import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import saf.components.AppWorkspaceComponent;
import mv.MapViewerApp;
import saf.ui.AppGUI; 
/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {
    MapViewerApp app;
    
    AppGUI gui;
    
    public Workspace(MapViewerApp initApp) {
        app = initApp;
        workspace = new Pane();
        gui = app.getGUI();
        ((FlowPane)gui.getAppPane().getTop()).getChildren().remove(0);
        ((FlowPane)gui.getAppPane().getTop()).getChildren().remove(1);
    }

    @Override
    public void reloadWorkspace() {
        
    }

    @Override
    public void initStyle() {
        
    }
}
